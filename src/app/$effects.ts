import { Injectable } from '@angular/core';
import 'rxjs/add/observable/of';

const LOADING_DEBOUNCE = 500;

@Injectable()
export class GlobalEffects {
    loadingDebounce = LOADING_DEBOUNCE;

    constructor(
    ) {}
}
