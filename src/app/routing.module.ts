import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorPageComponent } from 'app/pages/components/error-page/index';

const routes: Routes = [
    { path: '', loadChildren: 'app/pages/guest/main/main.module#MainModule' },
    { path: '**', component: ErrorPageComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})

export class AppRoutingModule { }
