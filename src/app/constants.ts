export const APP_LOADING_STATE_CHANGED = 'APP_LOADING_STATE_CHANGED';
export enum TypeEnum {
    'hot',
    'intermediate',
    'advanced',
    'hardcore',
    'academic',
}

export enum LanguageEnum {
    'ru',
    'en',
}

export interface ConferenceInterface {
    title: string;
    author: string;
    language: LanguageEnum;
    type: TypeEnum;
}

export interface FilterDataInterface {
    languages: LanguageEnum[];
    types: TypeEnum[];
    search: string;
}
