import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, enableProdMode } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './routing.module';
import { UIService } from './services/UIService';
import { AppComponent } from './app.component';
import { ErrorPageComponent } from './pages/components/error-page/index';
import { SimpleGuard } from './guards/SimpleGuard';

// enableProdMode();

@NgModule({
    imports: [
        FlexLayoutModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
        HttpModule,
        HttpClientModule,
        AppRoutingModule,
    ],
    declarations: [
        AppComponent,
        ErrorPageComponent,
    ],
    providers: [
        UIService,
        SimpleGuard,
    ],
    bootstrap: [AppComponent],
})
export class AppModule { }
