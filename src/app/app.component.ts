import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';

import { GlobalEffects } from './$effects';
import { mergeEffects } from '@ngrx/effects';
import { commonReducers, AppStateInterface } from './store';

@Component({
    selector: 'cnf-root',
    styles: [`
    `],
    template: `
    <div class="layout">
        <router-outlet></router-outlet>
    </div>`,
    providers: [GlobalEffects],
})

export class AppComponent implements OnInit, OnDestroy {
    private effectsSubscription: ISubscription;

    constructor(
        private store$: Store<AppStateInterface>,
        private effects$: GlobalEffects,
    ) {}

    ngOnInit(): void {
        this.effectsSubscription = mergeEffects(this.effects$).subscribe();

        Object.keys(commonReducers).forEach((key) => {
            this.store$.addReducer(key, commonReducers[key]);
        });

    }

    ngOnDestroy(): void {
        this.effectsSubscription.unsubscribe();
    }

}
