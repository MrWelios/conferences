import { ActionWithPayloadInterface } from 'app/reducers';
import {
    SET_CONFERENCE,
} from './constants';
import { ConferenceInterface } from 'app/constants';

export const ConferenceReducer = (state: ConferenceInterface[] = [], action: ActionWithPayloadInterface) => {
    if (action.type === SET_CONFERENCE) {
        return action.payload.data;
    }
    return state;
};
