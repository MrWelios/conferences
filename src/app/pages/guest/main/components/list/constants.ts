import { AppStateInterface } from 'app/store';
import { ConferenceInterface } from 'app/constants';

export interface ListStateInterface extends AppStateInterface {
    conference: ConferenceInterface[];
}
