import { Injectable } from '@angular/core';
import { debounceTime, tap, switchMap, catchError } from 'rxjs/operators';
import { Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import get from 'lodash/get';

import { GET_CONFERENCE } from '../../constants';
import { setAppLoadingState } from 'app/actions';
import { setConference } from '../../actions';
import { ActionWithPayloadInterface } from 'app/reducers';
import { MainGlobalEffects } from '../../$effects';
import { ConferenceInterface } from 'app/constants';

@Injectable()
export class ListEffects extends MainGlobalEffects {
    @Effect() requestGetConference = this.actions$.pipe(
        ofType(GET_CONFERENCE),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            return this.uiService.getConference().pipe(
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
                switchMap((response: ConferenceInterface[]) => {
                    this.store$.dispatch(setConference(response));
                    return of();
                }),
                catchError((error) => {
                    console.log(error);
                    return of(error);
                }),
            );
        }),
    );
}
