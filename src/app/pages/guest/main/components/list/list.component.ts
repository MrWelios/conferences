import { Component, OnInit, OnDestroy } from '@angular/core';
import { mergeEffects } from '@ngrx/effects';
import { ISubscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import cloneDeep from 'lodash/cloneDeep';

import { ListEffects } from './$effects';
import { getConference } from '../../actions';
import { ListStateInterface } from './constants';
import { ConferenceReducer } from '../../redusers';
import { ConferenceInterface, FilterDataInterface } from 'app/constants';

@Component({
    templateUrl: './list.template.html',
    styleUrls: [
        './list.style.scss',
    ],
    providers: [ListEffects],
})

export class ListComponent implements OnInit, OnDestroy {

    isLoading$: Observable<boolean>;
    data: ConferenceInterface[];
    displayData: ConferenceInterface[] = [];
    filterData: FilterDataInterface;

    readonly languages = [
        'ru',
        'en',
    ];

    readonly types = [
        'intermediate',
        'advanced',
        'hardcore',
        'academic',
    ];

    private effectsSubscription: ISubscription;
    private dataSubscription: ISubscription;

    constructor(
        private store$: Store<ListStateInterface>,
        private effects$: ListEffects,
    ) { }

    ngOnInit() {
        this.store$.addReducer('conference', ConferenceReducer);
        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.isLoading$ = this.store$.select(state => state.isLoading);

        this.dataSubscription = this.store$.select(state => state.conference)
            .pipe(filter(data => data !== undefined)).subscribe(
                (data) => {
                    this.data = cloneDeep(data);
                    this.filterChange({ label: 'reset' });
                },
            );

        this.getConference();
    }

    getConference() {
        this.store$.dispatch(getConference());
    }

    ngOnDestroy() {
        this.dataSubscription.unsubscribe();
        this.effectsSubscription.unsubscribe();
        this.store$.removeReducer('conference');
    }

    filterChange(event) {
        switch (event.label) {
            case 'languages':
            case 'types':
                const index = this.filterData[event.label].indexOf(event.value);
                if (index !== -1) {
                    this.filterData[event.label].splice(index, 1);
                    break;
                }
                this.filterData[event.label].push(event.value);
                break;
            case 'search':
                this.filterData[event.label] = event.value;
                break;
            case 'reset':
                this.filterData = {
                    languages: [],
                    types: [],
                    search: '',
                };
                break;
        }

        this.displayData = this.data
            .filter(row => this.filterData.languages.length === 0 || this.filterData.languages.indexOf(row.language) !== -1)
            .filter(row => this.filterData.types.length === 0 || this.filterData.types.indexOf(row.type) !== -1)
            .filter(row => row.author.toLowerCase().indexOf(this.filterData.search.toLowerCase()) !== -1 
                || row.title.toLowerCase().indexOf(this.filterData.search.toLowerCase()) !== -1);
    }

}
