import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { RoutingModule } from './routing.module';
import { ListComponent } from './components/list/list.component';
import { CnfCardModule } from 'app/widgets/card/card.module';
import { CnfFilterModule } from 'app/widgets/filter/filter.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RoutingModule,
        CnfCardModule,
        CnfFilterModule,
    ],
    providers: [
    ],
    declarations: [
        ListComponent,
    ],
})

export class MainModule {}
