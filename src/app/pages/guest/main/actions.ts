import { ActionWithPayloadInterface } from 'app/reducers';
import { 
    GET_CONFERENCE, 
    SET_CONFERENCE,
} from './constants';

import { ConferenceInterface } from 'app/constants';

export function getConference(): ActionWithPayloadInterface {
    return {
        type: GET_CONFERENCE,
    };
}

export function setConference(data: ConferenceInterface[]): ActionWithPayloadInterface {
    return {
        type: SET_CONFERENCE,
        payload: {
            data,
        },
    };
}
