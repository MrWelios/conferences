import { ActionWithPayloadInterface } from 'app/reducers';
import {
    APP_LOADING_STATE_CHANGED,
} from './constants';

export function setAppLoadingState(isLoading: boolean = true): ActionWithPayloadInterface {
    return {
        type: APP_LOADING_STATE_CHANGED,
        payload: {
            isLoading,
        },
    };
}
