import { routerReducer, RouterReducerState } from '@ngrx/router-store';

import {
    isLoading,
} from './reducers';

export interface AppStateInterface {
    isLoading: boolean;
    router: RouterReducerState;
}

export const commonReducers = {
    isLoading,
    router: routerReducer,
};
