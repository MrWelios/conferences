import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CnfCardComponent } from './card.component';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [CnfCardComponent],
    exports: [CnfCardComponent],
})

export class CnfCardModule {}
