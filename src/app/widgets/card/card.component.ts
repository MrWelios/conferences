import {
    Component,
    Input,
} from '@angular/core';
import { ConferenceInterface } from 'app/constants';

// @AutoUnsubscribe()
@Component({
    selector: 'cnf-cards',
    templateUrl: './card.template.html',
    styleUrls: ['./card.style.scss'],
})
export class CnfCardComponent {
    @Input() data: ConferenceInterface;

    constructor() {

    }

}
