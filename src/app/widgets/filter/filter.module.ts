import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CnfFilterComponent } from './filter.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
    ],
    declarations: [CnfFilterComponent],
    exports: [CnfFilterComponent],
})

export class CnfFilterModule {}
