import {
    Component,
    Input,
    Output,
    EventEmitter,
} from '@angular/core';

import { DataChangeInterface } from './filter.constants'; 
import { FilterDataInterface } from 'app/constants';

@Component({
    selector: 'cnf-filter',
    templateUrl: './filter.template.html',
    styleUrls: ['./filter.style.scss'],
})
export class CnfFilterComponent {

    @Input() languages: 'ru'|'en'[] = [];
    @Input() types: string[] = [];
    @Input() data: FilterDataInterface = {
        languages: [],
        types: [],
        search: '',
    };

    @Output() filterChange = new EventEmitter<DataChangeInterface>();

    constructor() {
    }

    change(filterType, element?) {
        this.filterChange.emit({
            label: filterType,
            value: element,
        });
    }

}
