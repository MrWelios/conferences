export interface DataChangeInterface {
    label: string;
    value: string;
}
