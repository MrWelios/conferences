import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { RestService } from './RestService';

@Injectable()
export class UIService extends RestService {
    
    constructor(
        httpClient: HttpClient,
    ) {
        super(httpClient);
    }

    getConference(): Observable<any> {
        return this.get('/assets/mock/response.json', {});
    }

    isEmpty(obj) {

        if (obj == null) {
            return true;
        }
    
        if (obj.length > 0) {
            return false;
        }
        if (obj.length === 0) {
            return true;
        }
    
        if (typeof obj !== 'object') {
            return true;
        }
    
        for (const key in obj) {
            if (key) {
                return false;
            }
        }
    
        return true;
    }
}
