import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import get from 'lodash/get';

import { environment } from 'environments/environment';

@Injectable()
export class RestService {

    constructor(
        private httpClient: HttpClient,
    ) { }

    protected get(url, data?, options?): Observable<any> {
        return this.httpClient.get(this.getUrl(url) + this.getUrlParams(data), options);
    }

    protected post(url, data?, options?): Observable<any> {
        return this.httpClient.post(this.getUrl(url), data, options);
    }

    protected patch(url, data?, options?): Observable<any> {
        return this.httpClient.patch(this.getUrl(url), data, options);
    }

    protected put(url, data?, options?): Observable<any> {
        return this.httpClient.put(this.getUrl(url), data, options);
    }

    protected delete(url, options?): Observable<any> {
        return this.httpClient.delete(this.getUrl(url), options);
    }

    private getUrl(url: string): string {
        let newUrl = url;
        if (newUrl.indexOf('http') === -1 && newUrl.indexOf('/assets') === -1) {
            newUrl = get(environment, 'api.url', '') + url;
        }
        return newUrl;
    }

    private getUrlParams(params): string {
        const resp = new HttpParams();
        Object.keys(params).forEach((key) => {
            resp.append(key, encodeURIComponent(params[key]));
        });
    
        return `?${resp.toString()}`;
    }

}
