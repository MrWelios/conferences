import { isObject } from 'rxjs/internal-compatibility';

export class UiUtilites {
    static sortObject(input, attribute) {
        if (!isObject(input)) {
            return input;
        }

        return Object.keys(input).sort((a, b) => {
            if (!input[a].hasOwnProperty(attribute)) {
                return -1;
            }
            if (!input[b].hasOwnProperty(attribute)) {
                return 1;
            }
            return parseInt(input[a][attribute], 10) - parseInt(input[b][attribute], 10);
        });
    }

    static sortArray(input, attribute) {
        return input.sort((a, b) => {
            if (a[attribute] > b[attribute]) {
                return 1;
            }
            if (a[attribute] < b[attribute]) {
                return -1;
            }
            return 0;
        });
    }
}
